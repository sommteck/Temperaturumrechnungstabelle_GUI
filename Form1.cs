using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Temperaturumrechnungstabelle_GUI
{
    public partial class Temperaturumrechnungstabelle : Form
    {
        public Temperaturumrechnungstabelle()
        {
            InitializeComponent();

            ToolTip tp = new ToolTip();
            tp.SetToolTip(cmd_clear, "Ein- und Ausgaben löschen");
            tp.SetToolTip(cmd_create, "Umrechnungstabelle erstellen");
            tp.SetToolTip(cmd_end, "Programm beenden");
            tp.SetToolTip(txt_von, "niedrigste Temperatur eingeben");
            tp.SetToolTip(txt_bis, "höchste Temperatur eingeben");
            tp.SetToolTip(dataGridView1, "umgerechnete Werte");
        }

        private void cmd_end_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void cmd_clear_Click(object sender, EventArgs e)
        {
            txt_von.Text = txt_bis.Text = null;
            dataGridView1.Rows.Clear();
            txt_von.Focus();
        }

        private void cmd_create_Click(object sender, EventArgs e)
        {
            double von, bis, fahrenheit, kelvin;
            try
            {
                von = Convert.ToDouble(txt_von.Text);
                bis = Convert.ToDouble(txt_bis.Text);
                for(; von <= bis; von = von +0.1)
                {
                    fahrenheit = 32 + 1.8 * von;
                    kelvin = 273.15 + von;
                    dataGridView1.Rows.Add(von.ToString(), fahrenheit.ToString("F2"), kelvin.ToString("F2"));
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                txt_von.Text = txt_bis.Text = null;
                txt_von.Focus();
            }
        }
    }
}