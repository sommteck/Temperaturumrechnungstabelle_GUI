namespace Temperaturumrechnungstabelle_GUI
{
    partial class Temperaturumrechnungstabelle
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txt_von = new System.Windows.Forms.TextBox();
            this.txt_bis = new System.Windows.Forms.TextBox();
            this.cmd_create = new System.Windows.Forms.Button();
            this.cmd_clear = new System.Windows.Forms.Button();
            this.cmd_end = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.SP_Celsius = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SP_Kelvin = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SP_Fahrenheit = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(26, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(107, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "von Grad Cesius";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(26, 82);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(106, 16);
            this.label2.TabIndex = 1;
            this.label2.Text = "bis Grad Celsius";
            // 
            // txt_von
            // 
            this.txt_von.Location = new System.Drawing.Point(185, 29);
            this.txt_von.Name = "txt_von";
            this.txt_von.Size = new System.Drawing.Size(100, 22);
            this.txt_von.TabIndex = 1;
            // 
            // txt_bis
            // 
            this.txt_bis.Location = new System.Drawing.Point(185, 82);
            this.txt_bis.Name = "txt_bis";
            this.txt_bis.Size = new System.Drawing.Size(100, 22);
            this.txt_bis.TabIndex = 2;
            // 
            // cmd_create
            // 
            this.cmd_create.Location = new System.Drawing.Point(416, 29);
            this.cmd_create.Name = "cmd_create";
            this.cmd_create.Size = new System.Drawing.Size(75, 23);
            this.cmd_create.TabIndex = 3;
            this.cmd_create.Text = "&Erstellen";
            this.cmd_create.UseVisualStyleBackColor = true;
            this.cmd_create.Click += new System.EventHandler(this.cmd_create_Click);
            // 
            // cmd_clear
            // 
            this.cmd_clear.Location = new System.Drawing.Point(416, 82);
            this.cmd_clear.Name = "cmd_clear";
            this.cmd_clear.Size = new System.Drawing.Size(75, 23);
            this.cmd_clear.TabIndex = 4;
            this.cmd_clear.TabStop = false;
            this.cmd_clear.Text = "&Löschen";
            this.cmd_clear.UseVisualStyleBackColor = true;
            this.cmd_clear.Click += new System.EventHandler(this.cmd_clear_Click);
            // 
            // cmd_end
            // 
            this.cmd_end.Location = new System.Drawing.Point(416, 141);
            this.cmd_end.Name = "cmd_end";
            this.cmd_end.Size = new System.Drawing.Size(75, 23);
            this.cmd_end.TabIndex = 5;
            this.cmd_end.TabStop = false;
            this.cmd_end.Text = "&Beenden";
            this.cmd_end.UseVisualStyleBackColor = true;
            this.cmd_end.Click += new System.EventHandler(this.cmd_end_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.SP_Celsius,
            this.SP_Kelvin,
            this.SP_Fahrenheit});
            this.dataGridView1.Location = new System.Drawing.Point(29, 141);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(343, 244);
            this.dataGridView1.TabIndex = 6;
            // 
            // SP_Celsius
            // 
            this.SP_Celsius.HeaderText = "°Celsius";
            this.SP_Celsius.Name = "SP_Celsius";
            // 
            // SP_Kelvin
            // 
            this.SP_Kelvin.HeaderText = "°Kelvin";
            this.SP_Kelvin.Name = "SP_Kelvin";
            // 
            // SP_Fahrenheit
            // 
            this.SP_Fahrenheit.HeaderText = "°Fahrenheit";
            this.SP_Fahrenheit.Name = "SP_Fahrenheit";
            // 
            // Temperaturumrechnungstabelle
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(546, 418);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.cmd_end);
            this.Controls.Add(this.cmd_clear);
            this.Controls.Add(this.cmd_create);
            this.Controls.Add(this.txt_bis);
            this.Controls.Add(this.txt_von);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "Temperaturumrechnungstabelle";
            this.Text = "Temperaturumstellungstabelle";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txt_von;
        private System.Windows.Forms.TextBox txt_bis;
        private System.Windows.Forms.Button cmd_create;
        private System.Windows.Forms.Button cmd_clear;
        private System.Windows.Forms.Button cmd_end;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn SP_Celsius;
        private System.Windows.Forms.DataGridViewTextBoxColumn SP_Kelvin;
        private System.Windows.Forms.DataGridViewTextBoxColumn SP_Fahrenheit;
    }
}

